/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pinkz7.javaswingcomponent;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author ripgg
 */
public class JOptionPaneExampleshowInputDialog3 {

    JFrame f;

    JOptionPaneExampleshowInputDialog3() {
        f = new JFrame();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        String name = JOptionPane.showInputDialog(f, "Enter Name");
    }

    public static void main(String[] args) {
        new JOptionPaneExampleshowInputDialog3();
    }
}
